#version 440

layout(location=0) out vec4 FragColor;

in float v_Grey;
in vec2 v_Tex;
in vec3 v_Norm;
in vec3 v_Pos;

uniform sampler2D u_Texture;
uniform sampler2D u_Texture2;
uniform sampler2D u_HeightMapTexture;
uniform float u_Time;
uniform vec2 u_Points[5];

const vec3 c_Light1 = vec3(0.f, -1.f, 0.3f);
const vec3 c_Camera = vec3(0.f, -1.f, 0.2f);

void Waves()
{
	vec2 newUV = v_Tex.xy;
	vec4 newColor= vec4(0);
	for(int i = 0; i < 5; i++)
	{
		vec2 newPoint = u_Points[i];
		vec2 newVec = newPoint - newUV;
		float distance = length(newVec) *  8 * 3.141592;
	
		newColor += vec4(sin(distance));
	}
	FragColor = vec4(v_Grey);
}

void main()
{
	//vec4 snowColor = texture(u_Texture, v_tex * 10.0) * v_Grey;
	//vec4 grassColor = texture(u_Texture2, v_tex * 10.0) * (1.0 - v_Grey);
	//vec4 finalColor = snowColor + grassColor;

	float a = 0.4f;
	float d = 0.5f;
	float s = 0.3f;

	vec3 LightDir = c_Light1 - v_Pos;
	vec3 ambient = vec3(1.0, 1.0, 1.0);
	float diffuse = clamp(dot(LightDir, v_Norm), 0.0 ,1.0);
	vec3 diffuseColor = vec3(1.0, 1.0, 1.0);

	vec3 reflectDir = reflect(LightDir, v_Norm);
	vec3 viewDir = v_Pos - c_Camera;
	vec3 specColor = vec3(1, 1, 1);
	float spec = clamp(dot(viewDir , reflectDir), 0.0, 1.0);
	spec = pow(spec, 6.0);

	vec3 newColor = ambient *  a + diffuseColor * diffuse * d + specColor * spec *s;

	FragColor = vec4(newColor, 1);
}
